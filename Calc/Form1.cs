﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc
{
    public partial class Form1 : Form
    {
        private double a, b, c;
        private char symbol;
        private bool result;

        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Black;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void buttonPlus_Click(object sender, EventArgs e)
        {
            do
            {
                result = double.TryParse(textBox.Text, out a);

                if (result == false)
                {
                    MessageBox.Show("Значение первого числа введено неверно! Повторите ввод");
                    textBox.Clear();
                    return;
                }
            }
            while (result == false);

            symbol = (sender as Button).Text[0];
            textBox.Clear();

        }

        private void buttonTods_Click(object sender, EventArgs e)
        {
            if (textBox.Text != " ")
            {
                textBox.Text += (sender as Button).Text;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            textBox.Text += (sender as Button).Text;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (textBox.Text == "")
            {
                textBox.Text += "-";
            }
        }

        private void BackSpace_Click(object sender, EventArgs e)
        {
            if(textBox.Text !="")
            {
                textBox.Text = textBox.Text.Remove(textBox.Text.Length-1);
            }
        }

        private void buttonAnswer_Click(object sender, EventArgs e)
        {
            do
            {
                result = double.TryParse(textBox.Text, out b);

                if (result == false)
                {
                    MessageBox.Show("Значение второго числа введено неверно! Повторите ввод.");
                    textBox.Clear();
                    return;
                }
            }
            while (result == false);

            switch (symbol)
            {
                case '+':
                    c = a + b;
                    break;

                case '-':
                    c = a - b;
                    break;

                case '/':
                    c = a / b;
                    break;

                case 'x':
                    c = a * b;
                    break;
            }

            textBox.Text = c.ToString();
        }

        private void button12_Click_1(object sender, EventArgs e)
        {
            textBox.Clear();
        }


    }
}
